# Ingénieur.e plateforme _DOMAINE-A-PRECISER_

## Contexte et atouts du poste

L'ingénieur.e recruté(e) s'intègre au collectif des ingénieurs.es permanents de l'institut, représenté au niveau d'un centre par le Service d'Expérimentation et de Développement (SED), et au niveau national par le Service du Développement Technologique (SDT).

_Préciser ici le rattachement hiérarchique : SED/RSED (par défaut) ou EPI/REP (à justifier)_

Son activité principale s'inscrit dans le cadre de la conception, la mise en œuvre, la conduite des évolutions et l'accompagnement des utilisateurs de plateformes matérielles et logicielles de l'institut.

_Préciser ici le contexte de la ou les plateformes concernée(s) (quelles EP utilisatrices, dans un contexte de partenariats ou pas, quels domaines scientifique, etc.)_

La première affectation au sein de cette plateforme porte sur une durée de _à préciser (4 par défaut)_ ans.

## Mission confiée

* Mission principale (environ 90 % de son temps)
  * Conception, développement et conduite d'évolutions de plateformes matérielles et logicielles dans le _DOMAINE-A-PRECISER_
  * Contribution à la conception et à la mise en œuvre des environnements physiques, systèmes et réseaux de la plateforme avec les services support qui en ont la charge
  * Conseil et soutien à l'expérimentation sur les plateformes du _DOMAINE-A-PRECISER_
  * Présentation et promotion des possibilités offertes des plateformes dans le _DOMAINE-A-PRECISER_ pour un public interne et externe 
  * Soutien et encadrement pour les développeurs de plateformes matérielles et logicielles dans le _DOMAINE-A-PRECISER_
  * Mission spécifique pour la première affectation : _remplir ici les missions spécifiques au poste_
* Missions collectives (environ 10 % de son temps) : 
  Dans le but de mutualiser son savoir-faire, la personne recrutée est amenée à réaliser des activités utiles au collectif des ingénieurs de développement de l'institut, dans son _DOMAINE-A-PRECISER_ mais aussi plus largement.

## Principales activités

* Activités principales
  * Conception et développement des logiciels scientifiques utiles à l'offre de service des plateformes dans le _DOMAINE-A-PRECISER_
  * Contribution à la conception et au développement des logiciels utiles au bon fonctionnement des plateformes dans le _DOMAINE-A-PRECISER_, en lien avec les ingénieurs.es de la DSI
  * Contribution à la conception globale, au maquettage et à la mise en œuvre des plateformes, en lien avec les autres services concernés (notamment la DSI) et les chercheurs.euses, sur l'ensemble des couches des plateformes :
    * infrastructure
    * matériel informatique et spécifique au domaine _DOMAINE-A-PRECISER_
    * système, réseau, logiciels de bas niveau
    * logiciel scientifique
    * cas d'usage d'expérimentation
  * Rédaction et présentation de documentation
  * Contribution aux expérimentations et publications scientifiques issues des projets sur lesquels la personne est affectée
  * Veille technologique, en particulier dans le domaine : état de l'art, développement et/ou déploiement de preuves de concept (PoC), ...
  * Réflexion, mise en place, et éventuellement coordination d'un mode de fonctionnement entre les développeurs.euses au sein des projets sur lesquels la personne est affectée
    * Présentation des évolutions et des choix techniques ;
    * Identification des besoins des utilisateurs ;
    * Roadmap de travail au fil de l'activité.
  * Mise en place de support de formation à destination des utilisateurs.trices - développeurs.euses ou expérimentateurs.trices 
  * Contribution au montage et au suivi de projets d'envergure, notamment pour financer les plateformes (CPER, Equipex, etc.)
  * Conseil et expertise en développement technologique auprès des membres de l'équipe / des équipes / du domaine
  * _Activités spécifiques_
* Activités collectives, par exemple : 
  * Formations ponctuelles, séminaires
  * Vecteur des bonnes pratiques en génie logiciel et en expérimentation
  * Aide aux recrutements et encadrement
  * Participation à des rédactions de projets, conseils sur des projets de développement
  * Représentation de l'institut sur le plan technique

## Compétences

* Connaissances solides et expérience en développement logiciel : 
  * maîtrise d'au moins 1 langage de programmation (C++, java, OCaml, Python, RUST, …), 
  * architecture logicielle et paradigmes de programmation, génie logiciel, bonnes pratiques et outils de développement logiciel (versionning, documentation, compilation, packaging, …),
  * développement fullstack (pour être en capacité de mettre à disposition les outils via des frontend-web).
* Connaissances et expérience dans _DOMAINE-A-PRECISER_, notamment en expérimentation scientifique (a minima pour les jeunes recrues, un potentiel à acquérir cela)
* Maîtrise d'un outillage spécifique propre aux plateformes du _DOMAINE-A-PRECISER_.
* Connaissances et expérience en maquettage, prototypage matériels et/ou logiciels
* Capacité à conduire la veille technologique au sein de l’institut
* Capacité à rédiger, à publier et à présenter en français et en anglais
* Encadrement technique d'ingénieurs.es
* Capacité à proposer et réaliser des mises en œuvre de référence, des prototypes et démonstrateurs : autonomie, créativité, veille proactive, écoute des besoins.
* Capacité à comprendre les contextes et besoins scientifiques, et à les traduire dans des implémentations technologiques.
* Capacités et expérience dans le choix et l'achat de matériel
* Connaissances de la démarche scientifique associée à l'expérimentation (science reproductible, état de l'art scientifique, état de l'art technologique d'un domaine,  publication logicielle, contribution à la publication scientifique sur l'aspect méthodologique et la mesure de performance).
* Savoir être : ténacité, aimant l'effort au long terme, ouverture d'esprit, bon relationnel pour former et aider les utilisateurs (stagiaires, doctorants).
* _Compétences spécifiques : Remplir ici les compétences spécifiques au poste_


