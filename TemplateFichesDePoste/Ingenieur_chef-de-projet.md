# Ingénieur chef de projet développement logiciel et expérimentation : _PROJET-A-PRECISER_

## Contexte

L'ingénieur recruté s'intègre au collectif des ingénieurs permanents de l'institut, représenté au niveau d'un centre par le Service d'Expérimentation et de Développement (SED).

_Préciser ici le rattachement hiérarchique : SED/RSED (par défaut) ou EPI/REP (à justifier)_

Son activité principale s'inscrit dans le cadre de projets d'envergure sur lesquels il est affecté pour une durée donnée, le plus souvent au sein d'une ou plusieurs équipes-projets.

_Préciser ici le contexte du projet concerné(s) \_PROJET-A-PRECISER_ (quelles EP utilisatrices, dans un contexte de partenariats ou pas, quels domaines scientifique, etc.)\_

La première affectation au sein de ce projet porte sur une durée de _à préciser (4 par défaut)_ ans.

## Mission

* Mission principale (environ 90 % de son temps)
  * Gestion de projet
  * Soutien et encadrement pour les développeurs
  * Contribution à la conception et développement de logiciels au sein des projets de développement sur lesquels la personne est affectée
  * Conseil et soutien à l'expérimentation
  * Mission spécifique pour la première affectation : _remplir ici les missions spécifiques au poste_
* Missions collectives (environ 10 % de son temps) : 
  Dans le but de mutualiser son savoir-faire, la personne recrutée est amenée à réaliser des activités utiles au collectif des ingénieurs de développement de l'institut, en particulier au regard du projet _PROJET-A-PRECISER_ mais aussi plus largement.

## Activités

* Activités principales
  * Analyse des besoins et formalisation sous forme de projet
  * Assurer la gestion de projet :
    * Suivi de l'atteinte des objectifs du projet :
      * Revue des réalisations techniques pour valider la conformité avec les objectifs
      * Définition et suivi des indicateurs
    * Mise en place et suivi des plannings et jalons
    * Management transversal des ingénieurs de développement et potentiellement d'autres contributeurs (ingénieurs DSI, chercheurs, etc.)
      * Calibrage de l'équipe et de ses compétences 
      * Recrutement
      * Accompagnement des ingénieurs et suivi des plans de charge
    * Suivi des budgets du projet
    * Reporting aux membres du projet, aux responsables fonctionnels, et plus largement
  * Contributions :
    * aux choix techniques, à la conception et au développement des logiciels utiles aux travaux de recherche dans le cadre du projet _PROJET-A-PRECISER_
    * aux expérimentations et publications scientifiques issues des projets de développement sur lesquels la personne est affectée
    * à la veille technologique : état de l'art, développement et/ou déploiement de preuves de concept (PoC), ...
  * Rédaction et présentation de documentation technique, réalisation de formation à destination des développeurs / utilisateurs dans le cadre du projet _PROJET-A-PRECISER_
  * _Activités spécifiques_
* Activités collectives, par exemple : 
  * Formation ponctuelle, séminaires
  * Vecteur des bonnes pratiques en génie logiciel et en expérimentation
  * Aide aux recrutements et encadrement
  * Participation à des rédactions de projets, conseils sur des projets de développement
  * Représentation de l'institut sur le plan technique

## Compétences

* Capacité à cumuler un rôle de responsable technique et de gestion de projet au sens large :
  * Définition/négociation d'objectifs au-delà des contrats en cours.
  * Contribution active à la recherche de moyens et à leur gestion.
  * Capacité à déléguer.
  * Suivi des réalisations et reporting.
* Connaissances et expérience en gestion de projet
* Connaissances solides et expérience en développement logiciel : 
  * maîtrise d'au moins 1 langage de programmation (C++, java, OCaml, Python, RUST, …), 
  * architecture logicielle et paradigmes de programmation, génie logiciel, bonnes pratiques et outils de développement logiciel (versionning, documentation, compilation, packaging, …)
* Encadrement technique d'autres ingénieurs
* Connaissances et/ou expérience dans un des domaines scientifiques d’Inria, notamment en expérimentation scientifique 
* Capacité à conduire la veille technologique au sein de l’institut
* Très bonne capacité à rédiger, à publier et à présenter en français et en anglais
* Capacité à comprendre les contextes et besoins scientifiques, et à les traduire dans des implémentations technologiques.
* Maîtrise de la démarche scientifique associée à l'expérimentation (science reproductible, état de l'art scientifique, état de l'art technologique d'un domaine,  publication logicielle, contribution à la publication scientifique sur l'aspect méthodologique et la mesure de performance).
* Capacité à représenter Inria et le projet en dehors d'Inria.
* Savoir être : bon relationnel, réseautage, planification.
* _Compétences spécifiques : remplir ici les compétences spécifiques au poste_


