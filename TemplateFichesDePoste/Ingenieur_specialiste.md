# Ingénieur développement logiciel spécialiste en _DOMAINE-A-PRECISER_

## Contexte

L'ingénieur recruté s'intègre au collectif des ingénieurs permanents de l'institut, représenté au niveau d'un centre par le Service d'Expérimentation et de Développement (SED).

_Préciser ici le rattachement hiérarchique : SED/RSED (par défaut) ou EPI/REP (à justifier)_

Son activité principale s'inscrit dans le cadre de projets d'envergure sur lesquels il est affecté pour une durée donnée, le plus souvent au sein d'une ou plusieurs équipes-projets.

_Préciser ici le contexte de l'EP concernée (ou des EP concernées) et / ou du domaine de spécialisation_

La première affectation au sein de cette équipe porte sur une durée de _à préciser (4 par défaut)_ ans.

## Mission

* Mission principale (environ 90 % de son temps)
  * Conception et développement de logiciels au sein des projets de développement sur lesquels la personne est affectée, essentiellement dans le domaine _DOMAINE-A-PRECISER_
  * Conseil et soutien à l'expérimentation dans le _DOMAINE-A-PRECISER_
  * Soutien et encadrement pour les développeurs dans le _DOMAINE-A-PRECISER_
  * Mission spécifique pour la première affectation : _remplir ici les missions spécifiques au poste_
* Missions collectives (environ 10 % de son temps) : 
  Dans le but de mutualiser son savoir-faire, la personne recrutée est amenée à réaliser des activités utiles au collectif des ingénieurs de développement de l'institut, dans son _DOMAINE-A-PRECISER_ mais aussi plus largement.

## Activités

* Activités principales
  * Conception et développement des logiciels scientifiques utiles aux travaux de recherche dans le _DOMAINE-A-PRECISER_
  * Rédaction et présentation de documentation
  * Contribution aux expérimentations et publications scientifiques issues des projets de développement sur lesquels la personne est affectée
  * Veille technologique, en particulier dans le domaine : état de l'art, développement et/ou déploiement de preuves de concept (PoC), ...
  * Réflexions, mise en place, et éventuellement coordination d'un mode de fonctionnement entre les développeurs au sein des projets de développement sur lesquels la personne est affectée
    * Présentation des évolutions et des choix techniques ;
    * Identification des besoins des utilisateurs ;
    * Roadmap de travail au fil de l'activité.
  * Mise en place de support de formation à destination des développeurs / utilisateurs au sein de l'équipe 
  * Conseil et expertise en développement technologique auprès des membres de l'équipe / des équipes / du domaine
  * _Activités spécifiques_
* Activités collectives, par exemple : 
  * Formation ponctuelle, séminaires
  * Vecteur des bonnes pratiques en génie logiciel et en expérimentation
  * Aide aux recrutements et encadrement
  * Participation à des rédactions de projets, conseils sur des projets de développement
  * Représentation de l'institut sur le plan technique

## Compétences

* Expertise dans _DOMAINE-A-PRECISER_, notamment en expérimentation scientifique (a minima pour les jeunes recrues, un potentiel à acquérir cela)
* Connaissances solides et expérience en développement logiciel : 
  * maîtrise d'au moins 1 langage de programmation (C++, java, OCaml, Python, RUST, …), 
  * architecture logicielle et paradigmes de programmation, génie logiciel, bonnes pratiques et outils de développement logiciel (versionning, documentation, compilation, packaging, CI, CD …)
* Connaissances et expérience en maquettage, prototypage matériels et/ou logiciels
* Capacité à conduire la veille technologique au sein de l’institut
* Capacité à rédiger, à publier et à présenter en français et en anglais
* Encadrement technique d'autres ingénieurs
* Capacité à proposer et réaliser des mises en œuvre de référence, des prototypes et démonstrateurs : autonomie, créativité, veille proactive, écoute des besoins.
* Capacité à comprendre les contextes et besoins scientifiques, et à les traduire dans des implémentations technologiques.
* Maîtrise de la démarche scientifique associée à l'expérimentation (science reproductible, état de l'art scientifique, état de l'art technologique d'un domaine,  publication logicielle, contribution à la publication scientifique sur l'aspect méthodologique et la mesure de performance).
* Savoir être : ténacité, aimant l'effort au long terme, ouverture d'esprit.
* _Compétences spécifiques : Remplir ici les compétences spécifiques au poste_
* Expertise technologique pointue sur au moins un outil technologique du domaine scientifique _DOMAINE-A-PRECISER_ _, par exemple : librairie spécifique (e.g. TensorFlow, PETSc, Eclipse, ...), chaîne de compilation spécifique (e.g. LLVM), architecture spécifique (e.g. FPGA), ..._.


